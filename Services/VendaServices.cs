using tech_test_payment_api.Enums;
using tech_test_payment_api.Models;

namespace ServicesAPI.Services
{
    public static class VendaServices
    {
        public static bool AtualizarStatusVenda(StatusVenda status, Venda venda){
            
            switch(venda.Status){
                case StatusVenda.Aguardando_Pagamento:
                    if (status != StatusVenda.Pagamento_Aprovado && status != StatusVenda.Cancelado) 
                        return false;
                    break;
                case StatusVenda.Pagamento_Aprovado:
                    if (status != StatusVenda.Enviado && status != StatusVenda.Cancelado) 
                        return false;
                    break;
                case StatusVenda.Enviado:
                    if (status != StatusVenda.Entregue) return false;
                    break;
               default:
                    return false;
            }

            venda.Status = status;
            return true;
        }
    }
}