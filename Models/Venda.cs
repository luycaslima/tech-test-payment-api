using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public virtual Vendedor Vendedor { get; set; }
        [Required]
        public virtual required ICollection<ItemCarrinho> Carrinho { get; set; }
        public DateTime DataVenda { get; set; }
        public StatusVenda Status { get;set; }
    }
}