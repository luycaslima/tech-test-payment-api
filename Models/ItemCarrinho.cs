using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class ItemCarrinho
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public required int Quantidade { get; set; }
        [Required]
        public required string Produto { get; set; }// Pode ser um virtual objeto Produto relaçao 1-1
    }
}