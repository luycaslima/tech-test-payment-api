namespace tech_test_payment_api.Enums
{
    public enum StatusVenda
    {
        Aguardando_Pagamento = 0,
        Pagamento_Aprovado = 1,
        Enviado = 2,
        Entregue = 3,
        Cancelado = 4
    }
}