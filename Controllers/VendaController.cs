using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServicesAPI.Services;
using tech_test_payment_api.Context;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        public readonly SistemaVendasContext _context;

        public VendaController(SistemaVendasContext context){
            _context = context;
        }

        [HttpGet("GetVenda/{id}")]
        public IActionResult GetVenda(int id){
            var venda = _context.Vendas.Include(venda => venda.Vendedor).Include(venda=> venda.Carrinho).FirstOrDefault(v=> v.Id == id);
            if (venda == null){
                return NotFound();

            }
            return Ok(venda);
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult PostVenda(Venda vendaFeita){

            vendaFeita.Status = StatusVenda.Aguardando_Pagamento;
            vendaFeita.DataVenda = DateTime.Now;

            if (vendaFeita.Carrinho.Count == 0) return BadRequest(new { Erro = "O carrinho não pode ser vazio." });

            foreach(var item in vendaFeita.Carrinho){
                if (item.Quantidade <= 0) return BadRequest(new { Erro = "A quantidade de itens de um item não pode ser menor igual a zero." });
            }

            _context.Vendas.Add(vendaFeita);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetVenda), new { id = vendaFeita.Id }, vendaFeita);
        }

        [HttpPut("AtualizarStatusVenda/{id}")]
        public IActionResult UpdateStatusVenda(int id, StatusVenda status){
            var vendaBanco = _context.Vendas.Find(id);
            if (vendaBanco == null){
                return NotFound();
            }
            
            var ePossivel = VendaServices.AtualizarStatusVenda(status, vendaBanco);
            if (!ePossivel){
                return BadRequest(new {Erro = "Transição de status inválida.", Status= status , StatusAtual = vendaBanco.Status });
            }

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(new {Status = "Novo Status Aplicado!"});
        }
    
    }
}